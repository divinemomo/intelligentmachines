package jay.iot.bootstrap;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;

import jay.iot.bootstrap.db.MBootstrap;
import jay.iot.commons.BootStrapClientObj;


public class ServiceAPI {

	public String fetchValue(HashMap<String, HashMap<String, String>> mReqParameters, String string) {
		String value = "hey0";//mReqParameters.get(string).get(string);
		if(value != null && value.length() != 0 )
			return value;
		return "";
	}

	public HashMap<String, HashMap<String, String>> parseRequest(String request) {
		// TODO Auto-generated method stub
		return null;
	}


	public HashMap<String, String> parseHeaders(HttpServletRequest contxt) {
		// TODO Auto-generated method stub
		return null;
	}


	public HashMap<String, String> parseBody(String bodyRequest) {
		
		HashMap< String,String > body = new HashMap<String,String>();
		try{body = new ObjectMapper().readValue(bodyRequest, body.getClass());}catch(Exception e) { e.printStackTrace();}
		return body;
	}

	public boolean validateClient(HashMap<String, String> mReqParameters) {

		BootStrapClientObj bClient = getDBRecord(mReqParameters.get("serialID"));
		if(bClient == null)
			return false;
		if (!bClient.getFirmware_version().equalsIgnoreCase(mReqParameters.get("firmwareVersion")))
			return false; // Handle upgradation
		if (!bClient.getPresent_location().equalsIgnoreCase(mReqParameters.get("presentLocation")))
			//return false; //check if the device needs an update.
			return true;
		return true;
	}

	private BootStrapClientObj getDBRecord(String serialID) {

		HashMap hm = new HashMap<String,String>();
		hm.put("resource_id", serialID);
		return new MBootstrap().getMDocument(hm);
	}

	public String getServerURL(HashMap<String, String> mReqParameters) {
		
		String country = mReqParameters.get("presentLocation");
		/*try {
			String re =  Countries.class.getDeclaredField(country).toString();
			return re;
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		return Countries.getCountry(country);
	}

	


}
