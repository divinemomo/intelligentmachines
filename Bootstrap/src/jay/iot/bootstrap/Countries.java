package jay.iot.bootstrap;

public class Countries {

	public final static String US = "http://localhost:8090/Server1/a/registerClient";
	
	public final static String Canada = "http://localhost:8095/Server2/b/registerClient/post";

	public static String getCountry(String country) {
		if("US".equalsIgnoreCase(country))
			return US;
		else if("Canada".equalsIgnoreCase(country))
			return Canada;
		return null;
	}
}
