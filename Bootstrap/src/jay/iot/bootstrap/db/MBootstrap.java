package jay.iot.bootstrap.db;

import java.util.HashMap;

import org.bson.BSON;
import org.bson.BSONObject;
import org.bson.Document;
import org.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import jay.iot.commons.BootStrapClientObj;
import jay.iot.commons.ClientRegDetails;

public class MBootstrap {

	public final String db = "273bootstrap";
	
	public final String collection = "registry";
	
	public BootStrapClientObj getMDocument(HashMap input) {
		MongoClient mongoC = new MongoClient("127.0.0.1", 27017);
		 MongoDatabase mongoDB = mongoC.getDatabase(db);
		 MongoCollection<Document> mongoCollection = mongoDB.getCollection(collection);
		 FindIterable<Document> document = mongoCollection.find(new BasicDBObject(input));
		 System.out.println(document.first());
		 BootStrapClientObj reg = null;
		 try {
		 reg = (new ObjectMapper()).
				 readValue(document.first().toJson().replace("$oid", "oid"), BootStrapClientObj.class);
		//				 readValue(document.first().toJson().replace("$oid", "oid"), ClientRegDetails.class);

		 }
		 catch(Exception e) {
			e.printStackTrace(); 
		 }
		 finally{
			 mongoC.close();
		 }
		 return reg;
	}

	public MBootstrap() {
		super();
		// TODO Auto-generated constructor stub
	}
}
