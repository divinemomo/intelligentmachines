package jay.iot.bootstrap;

import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import jay.iot.commons.PHResponse;


@Path("acknowledgeclient")
public class BootstrapServer {

	
	@POST
    @Path("/post")
	@Produces("application/json")
	@Consumes("application/json")
    public Response postStrMsg(String request) {
		
		System.out.println("Request-URL:"+request);
		ServiceAPI mService = new ServiceAPI();
		HashMap<String, String> mReqParameters = mService.parseBody(request.toString());
		boolean goodClient = mService.validateClient(mReqParameters);
		PHResponse resp= new PHResponse();
		if(goodClient){
			resp.body.put("serverURL", mService.getServerURL(mReqParameters));
			resp.status = 200;
		}
		else{
			resp.status = 404;
			resp.body.put("Error", "Rogue Client");
			//actions to take
		}
       // return Response.status(resp.status).entity(mService.toJson(resp.body)).build();
		return resp.build();
    }

}
