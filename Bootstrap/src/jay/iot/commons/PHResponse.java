package jay.iot.commons;

import java.util.HashMap;

import javax.ws.rs.core.Response;

import org.json.JSONObject;

public class PHResponse {

	public PHResponse (){ 
		body = new HashMap<String, String>();
	}
	
	public int status;
	
	public HashMap<String,String> body;
	
	public Response build() {
		
		return Response.status(status).entity(toJson(body)).build();
		
	}
	public String toJson(HashMap<String, String> body) {
		
		return new JSONObject(body).toString();
	}
}
