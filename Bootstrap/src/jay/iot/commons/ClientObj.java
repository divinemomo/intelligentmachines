package jay.iot.commons;

public class ClientObj {

	private ClientRegDetails regDetails;
	private PowerSource powerSource;
	private String batteryLevel;
	private String memoryFree;
	private String errors;
	private String currentTime;
	private String Binding;
	
	public PowerSource getPowerSource() {
		return powerSource;
	}
	public void setPowerSource(PowerSource powerSource) {
		this.powerSource = powerSource;
	}
	public String getBatteryLevel() {
		return batteryLevel;
	}
	public void setBatteryLevel(String batteryLevel) {
		this.batteryLevel = batteryLevel;
	}
	public String getMemoryFree() {
		return memoryFree;
	}
	public void setMemoryFree(String memoryFree) {
		this.memoryFree = memoryFree;
	}
	public String getErrors() {
		return errors;
	}
	public void setErrors(String errors) {
		this.errors = errors;
	}
	public String getCurrentTime() {
		return currentTime;
	}
	public void setCurrentTime(String currentTime) {
		this.currentTime = currentTime;
	}
	public String getBinding() {
		return Binding;
	}
	public void setBinding(String binding) {
		Binding = binding;
	}
}
