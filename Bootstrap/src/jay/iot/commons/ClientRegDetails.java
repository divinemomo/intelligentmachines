package jay.iot.commons;

public class ClientRegDetails {

	private ID _id;
	private String manufacturer;
	private String modelNumber;
	private String serialNumber;
	private String firmwareVersion;
	
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getModelNumber() {
		return modelNumber;
	}
	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getFirmwareVersion() {
		return firmwareVersion;
	}
	public void setFirmwareVersion(String frimwareVersion) {
		this.firmwareVersion = frimwareVersion;
	}
	public ID get_id() {
		return _id;
	}
	public void set_id(ID _id) {
		this._id = _id;
	}

}
