package jay.iot.rabbitmq;

import java.io.IOException;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;

public class RabbitMessage {


    public boolean pushMQ(String EXCHANGE_NAME,String message){
    	try {
	        ConnectionFactory factory = new ConnectionFactory();
	        factory.setHost("localhost");
	        Connection connection = factory.newConnection();
	        Channel channel = connection.createChannel();
	
	        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
	
	
	        channel.basicPublish(EXCHANGE_NAME, "", null, message.getBytes());
	        System.out.println(" [x] Sent '" + message + "'");
	
	        channel.close();
	        connection.close();
	        return true;
	    }
	    catch(Exception e){
	    	e.printStackTrace();
	    	return false;
	    }
     }
}