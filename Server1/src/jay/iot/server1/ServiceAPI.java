package jay.iot.server1;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;

import jay.iot.server1.db.MServer;



public class ServiceAPI {

	private String communicationurl = "http://localhost:8090/Server1/a/client/post";
	private String unregisterUrl = "http://localhost:8090/Server1/a/registerClient/delServer";
	public String fetchValue(HashMap<String, String> mReqParameters, String string) {
		if(mReqParameters == null)
			return "";
		String value = mReqParameters.get(string);
		if(value != null && value.length() != 0 )
			return value;
		return "";
	}



	public HashMap<String, String> parseBody(String bodyRequest) {
		
		HashMap< String,String > body = new HashMap<String,String>();
		try{body = new ObjectMapper().readValue(bodyRequest, body.getClass());}catch(Exception e) { e.printStackTrace();}
		return body;
	}

	public boolean registerClient(HashMap<String, String> mReqParameters) {
		// TODO Auto-generated method stub
		
		return new MServer().createRecordFromMap(mReqParameters,"server1", (String)mReqParameters.get("serialNumber"));
	}



	public HashMap<String, String> getServerDetails() {
		HashMap<String, String> serverDetails = new MServer().readRecord(HashMap.class, "server1", "personal");
		serverDetails.put("unregisterUrl", unregisterUrl);
		serverDetails.put("communicationurl",communicationurl);
		return serverDetails;
	}



	public boolean unRegisterClient(HashMap<String, String> mReqParameters) {

		return new MServer().deleteRecord("",mReqParameters.get("serialNumber"));
	}

	
}
