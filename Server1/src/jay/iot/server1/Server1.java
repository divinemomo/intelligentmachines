package jay.iot.server1;

import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import jay.iot.commons.PHResponse;
import jay.iot.server1.db.MServer;


@Path("registerClient")
public class Server1 {

	public static boolean observe = false;
	
	@POST
    @Path("/post")
	@Produces("application/json")
	@Consumes("application/json")
    public Response postStrMsg(String request) {
		
		PHResponse resp = new PHResponse();
		System.out.println("ResponseL:"+request);
		ServiceAPI mService = new ServiceAPI();
		HashMap<String, String> mReqParameters = mService.parseBody(request.toString());
		HashMap<String,String> serverDetails =	mService.getServerDetails();
		if(mService.registerClient(mReqParameters)) {
			resp.status = 200;
			resp.body.putAll(serverDetails);
		}
		else{
			resp.status = 404;
			resp.body.put("Error", "Rogue Client");
			//actions to take
		}
       // return Response.status(resp.status).entity(mService.toJson(resp.body)).build();
		return resp.build();
    }
	
	@POST
    @Path("/postTemperature")
	@Produces("application/json")
	@Consumes("application/json")
    public Response postTemperature(String request) {
		
			System.out.println("Observing requests ----- \n "+request +"\n");

		PHResponse resp = new PHResponse();
		ServiceAPI mService = new ServiceAPI();
		HashMap<String, String> mReqParameters = mService.parseBody(request.toString());
		boolean isOK = true;
		//isOK = new MServer().insertRecord(mReqParameters, "temperature");
		if(isOK) {
			resp.status = 200;
			resp.body.put("Status","OK");
		}
		else{
			resp.status = 404;
			resp.body.put("Error", "Rogue Client");
			//actions to take
		}
		return resp.build();
    }
	
	@POST
    @Path("/delServer")
	@Produces("application/json")
	@Consumes("application/json")
    public Response deleteRegistry(String request) {
		
		PHResponse resp = new PHResponse();
		System.out.println("Request -- "+request);
		ServiceAPI mService = new ServiceAPI();
		HashMap<String, String> mReqParameters = mService.parseBody(request.toString());
		//HashMap<String,String> serverDetails =	mService.getServerDetails();
		if(mService.unRegisterClient(mReqParameters)) {
			resp.status = 200;
			resp.body.put("Message", "Success");
		}
		else{
			resp.status = 404;
			resp.body.put("Error", "Rogue Client");
			//actions to take
		}
		return resp.build();
    }

	
	@POST
    @Path("/update")
	@Produces("application/json")
	@Consumes("application/json")
    public Response updateClient(String request) {
		
		PHResponse resp = new PHResponse();
		System.out.println("Request -- "+request);
		ServiceAPI mService = new ServiceAPI();
		HashMap<String, String> mReqParameters = mService.parseBody(request.toString());
		//HashMap<String,String> serverDetails =	mService.getServerDetails();
		if(mService.unRegisterClient(mReqParameters)) {
			resp.status = 200;
			resp.body.put("Message", "Success");
		}
		else{
			resp.status = 404;
			resp.body.put("Error", "Rogue Client");
			//actions to take
		}
		return resp.build();
    }

}
