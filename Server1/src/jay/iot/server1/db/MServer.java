package jay.iot.server1.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

public class MServer {

	private final String databaseName = "server1";
	
	public MServer() {
		
	}
	
	public boolean deleteRecord(String databaseName , String collection) {
		MongoClient mongoC = new MongoClient("localhost", 27017);
		
		try{
			MongoDatabase mongoDB = mongoC.getDatabase(databaseName);
			 MongoCollection<Document> mongoCollection = mongoDB.getCollection(collection);
			 mongoCollection.drop();
		}
		catch(Exception e) { e.printStackTrace(); return false; }
		finally{ mongoC.close(); }
		return true;
	}
	
	public < T > T  readRecord(Class<T> class1, String databaseName , String collection){
		
		MongoClient mongoC = new MongoClient("localhost", 27017);
		MongoDatabase mongoDB = mongoC.getDatabase(databaseName);
		MongoCollection<Document> mongoCollection = mongoDB.getCollection(collection);
		FindIterable<Document> cursor = mongoCollection.find();
		MongoCursor<Document> mongoCursor = cursor.iterator();
		T reg = null;
		try {
		    if(mongoCursor.hasNext()) {
		       Document obj = mongoCursor.next();
		       reg = (T) (new ObjectMapper()).
						 readValue(obj.toJson().replace("$oid", "oid"), 
								 class1);
		    }
		 }
		 catch(Exception e) {
			e.printStackTrace(); 
		 }
		 finally{
			 mongoC.close();
		 }
		return reg;
	}
	
	public < T > List<T>  readMultipleRecords(Class<T> className, String databaseName , String collection){
		
		MongoClient mongoC = new MongoClient("localhost", 27017);
		MongoDatabase mongoDB = mongoC.getDatabase(databaseName);
		MongoCollection<Document> mongoCollection = mongoDB.getCollection(collection);
		FindIterable<Document> cursor = mongoCollection.find();
		MongoCursor<Document> mongoCursor = cursor.iterator();
		List<T> reg= new ArrayList<T>();
		try {
		    while(mongoCursor.hasNext()) {
		       Document obj = mongoCursor.next();
		       T classObj = (T) (new ObjectMapper()).
						 readValue(obj.toJson().replace("$oid", "oid"), 
								 className);
		       reg.add(classObj);
		    }
		 }
		 catch(Exception e) {
			e.printStackTrace(); 
		 }
		 finally{
			 mongoC.close();
		 }
		return reg;
	}
	public <T> boolean createRecord(T inputRecord, String dB , String collection ) {

		JSONObject jObj = new JSONObject(inputRecord);

		MongoClient mongoC = new MongoClient("localhost", 27017);

		try{
		 MongoDatabase mongoDB = mongoC.getDatabase(dB);
		 MongoCollection<Document> mongoCollection = mongoDB.getCollection(collection);
		 mongoCollection.insertOne(Document.parse(jObj.toString()));
		}
		catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		finally{
			mongoC.close();
		}
		return true;
	}
	
	public <T> boolean createRecordFromMap(Map<String,String> inputRecord, String dB , String collection ) {

		MongoClient mongoC = new MongoClient("localhost", 27017);
		try{
		 MongoDatabase mongoDB = mongoC.getDatabase(dB);
		 MongoCollection<Document> mongoCollection = mongoDB.getCollection(collection);
		 
		 Map<String,Object> inserter = new HashMap<String,Object>();
		 inserter.putAll(inputRecord);
		 mongoCollection.insertOne(new Document(inserter));
		}
		catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		finally{
			mongoC.close();
		}
		return true;
	}
	
	public <T> boolean updateRecord (String databaseName , String collection , String updateKey, String updateValue) {
		
		MongoClient mongoC = new MongoClient("localhost", 27017);

		try{
			MongoDatabase mongoDB = mongoC.getDatabase(databaseName);
			MongoCollection<Document> mongoCollection = mongoDB.getCollection(collection);
			FindIterable<Document> document = mongoCollection.find();
			JSONObject jSON = new JSONObject(document.first());
			jSON.put(updateKey, updateValue);
			jSON.remove("_id");
			mongoDB.getCollection(collection).updateOne(document.first(),
			        new Document("$set", new Document(Document.parse(jSON.toString()))));
			return true;
		}
		catch(Exception e) { e.printStackTrace(); return false;}
		finally{
			mongoC.close();
		}
		
	}
}
