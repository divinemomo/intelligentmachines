package jay.iot.server1;

import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import jay.iot.commons.PHResponse;

@Path("mssg")
public class MessageO {

	//http://localhost:8090/Server1/a/mssg/messageOperation
		@POST
	    @Path("/messageOperation")
		@Produces("application/json")
		@Consumes("application/json")
	    public Response messageOperation(String request) {
			
			PHResponse resp = new PHResponse();
			System.out.println("ResponseL:"+request);
			
				resp.status = 200;
				resp.body.put("","Success");
			return resp.build();
	    }

}
