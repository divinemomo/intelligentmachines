package jay.iot.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jay.iot.rabbitmq.RabbitMessage;

public class ServletInterface extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//super.doGet(req, resp);
		System.out.println("Input methoed is :-- " + req.getQueryString());		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String paramServerID = req.getParameter("serverID");
		String query = req.getParameter("query");
		System.out.println(paramServerID +" :: "+ query);	
		if(query.contains("operation:writeAll")){
			query = query.replace("operation:writeAll", "operation:write");
			new RabbitMessage().pushMQ(paramServerID, "{operation:observe,value:cancel}");
		}
		boolean isSuccess = new RabbitMessage().pushMQ(paramServerID, query);
		System.out.println(isSuccess);
	}
}
