package jay.iot.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jay.iot.commons.PaymentObject;
import jay.iot.server1.db.MServer;

public class Payment extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		/*// TODO Auto-generated method stub
		super.doGet(req, resp);*/
		System.out.println("inside payment");
		
		List<PaymentObject> payList = new ArrayList<PaymentObject> ();
		
		payList = new MServer().readMultipleRecords(PaymentObject.class,"server1", "payment");
		System.out.println(payList);
		System.out.println(payList.get(0));
		req.setAttribute("payList", payList);
		
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/listpay");
		dispatcher.forward(req, resp);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.doPost(req, resp);
	}

}
