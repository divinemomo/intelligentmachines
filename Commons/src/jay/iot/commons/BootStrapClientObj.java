package jay.iot.commons;

public class BootStrapClientObj {

	private ID id;
	private String resource_id;
	private String firmware_version;
	private int year_of_manufacturing;
	private String sold_location;
	private String present_location;
	
	public String getResource_id() {
		return resource_id;
	}
	public void setResource_id(String resource_id) {
		this.resource_id = resource_id;
	}
	public String getFirmware_version() {
		return firmware_version;
	}
	public void setFirmware_version(String firmware_version) {
		this.firmware_version = firmware_version;
	}
	public int getYear_of_manufacturing() {
		return year_of_manufacturing;
	}
	public void setYear_of_manufacturing(int year_of_manufacturing) {
		this.year_of_manufacturing = year_of_manufacturing;
	}
	public String getSold_location() {
		return sold_location;
	}
	public void setSold_location(String sold_location) {
		this.sold_location = sold_location;
	}
	public String getPresent_location() {
		return present_location;
	}
	public void setPresent_location(String present_location) {
		this.present_location = present_location;
	}
	public ID getId() {
		return id;
	}
	public void setId(ID id) {
		this.id = id;
	}
	
}
