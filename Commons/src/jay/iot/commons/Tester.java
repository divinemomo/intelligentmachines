package jay.iot.commons;

import java.util.Random;

public class Tester {

	public static void main(String[] args) {
		String output = "";
		for(int i = 0 ; i < 60 ; i++) {
			output += "\"day "+i+"\" : \""+Math.random()+"\",";
		}
		System.out.println(output);
	}
}
