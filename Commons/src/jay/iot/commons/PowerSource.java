package jay.iot.commons;

public class PowerSource {

	private String powersource_main;
	private String powersource_USB;
	
	private String powersourceVolt_main;
	private String powersourceVolt_USB;
	
	private String powersourceCurrent_main;
	private String powersourceCurrent_USB;
	
	public String getPowersource_main() {
		return powersource_main;
	}
	public void setPowersource_main(String powersource_main) {
		this.powersource_main = powersource_main;
	}
	public String getPowersource_USB() {
		return powersource_USB;
	}
	public void setPowersource_USB(String powersource_USB) {
		this.powersource_USB = powersource_USB;
	}
	public String getPowersourceVolt_main() {
		return powersourceVolt_main;
	}
	public void setPowersourceVolt_main(String powersourceVolt_main) {
		this.powersourceVolt_main = powersourceVolt_main;
	}
	public String getPowersourceVolt_USB() {
		return powersourceVolt_USB;
	}
	public void setPowersourceVolt_USB(String powersourceVolt_USB) {
		this.powersourceVolt_USB = powersourceVolt_USB;
	}
	public String getPowersourceCurrent_main() {
		return powersourceCurrent_main;
	}
	public void setPowersourceCurrent_main(String powersourceCurrent_main) {
		this.powersourceCurrent_main = powersourceCurrent_main;
	}
	public String getPowersourceCurrent_USB() {
		return powersourceCurrent_USB;
	}
	public void setPowersourceCurrent_USB(String powersourceCurrent_USB) {
		this.powersourceCurrent_USB = powersourceCurrent_USB;
	}
	
}
