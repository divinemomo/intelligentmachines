package jay.iot.commons;

public class ClientObj {

	private ID _id;
	private String manufacturer;
	private String modelNumber;
	private String serialNumber;
	private String firmwareVersion;
	
	private String batterylevel;
	private String memoryfree;
	private String errorcode;
	private String currentTime;
	private String UTCOffset;
	private String Binding;
	
	private String powersource;
	private String powersource_USB;
	private String powersourceVolt_main;
	private String powersourceVolt_USB;
	private String powersourceCurrent_main;
	private String powersourceCurrent_USB;
	
	
	public String getBatterylevel() {
		return batterylevel;
	}
	public void setBatterylevel(String batterylevel) {
		this.batterylevel = batterylevel;
	}
	public String getCurrentTime() {
		return currentTime;
	}
	public void setCurrentTime(String currentTime) {
		this.currentTime = currentTime;
	}
	public String getBinding() {
		return Binding;
	}
	public void setBinding(String binding) {
		Binding = binding;
	}
	public ID get_id() {
		return _id;
	}
	public void set_id(ID _id) {
		this._id = _id;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getModelNumber() {
		return modelNumber;
	}
	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getFirmwareVersion() {
		return firmwareVersion;
	}
	public void setFirmwareVersion(String firmwareVersion) {
		this.firmwareVersion = firmwareVersion;
	}
	
	public String getPowersource_USB() {
		return powersource_USB;
	}
	public void setPowersource_USB(String powersource_USB) {
		this.powersource_USB = powersource_USB;
	}
	public String getPowersourceVolt_main() {
		return powersourceVolt_main;
	}
	public void setPowersourceVolt_main(String powersourceVolt_main) {
		this.powersourceVolt_main = powersourceVolt_main;
	}
	public String getPowersourceVolt_USB() {
		return powersourceVolt_USB;
	}
	public void setPowersourceVolt_USB(String powersourceVolt_USB) {
		this.powersourceVolt_USB = powersourceVolt_USB;
	}
	public String getPowersourceCurrent_main() {
		return powersourceCurrent_main;
	}
	public void setPowersourceCurrent_main(String powersourceCurrent_main) {
		this.powersourceCurrent_main = powersourceCurrent_main;
	}
	public String getPowersourceCurrent_USB() {
		return powersourceCurrent_USB;
	}
	public void setPowersourceCurrent_USB(String powersourceCurrent_USB) {
		this.powersourceCurrent_USB = powersourceCurrent_USB;
	}
	public String getPowersource() {
		return powersource;
	}
	public void setPowersource(String powersource) {
		this.powersource = powersource;
	}
	public String getMemoryfree() {
		return memoryfree;
	}
	public void setMemoryfree(String memoryfree) {
		this.memoryfree = memoryfree;
	}
	public String getErrorcode() {
		return errorcode;
	}
	public void setErrorcode(String errorcode) {
		this.errorcode = errorcode;
	}
	public String getUTCOffset() {
		return UTCOffset;
	}
	public void setUTCOffset(String UTCOffset) {
		this.UTCOffset = UTCOffset;
	}
}
